package com.vpn.IpKeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpKeeperApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpKeeperApplication.class, args);
	}

}
