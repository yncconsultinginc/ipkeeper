package com.vpn.IpKeeper.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

@RestController
@RequestMapping("/ipkeeper/")
public class IpKeeperController {

	private Gson gson = new Gson();
	private Map<String, String> sourceToIp = new HashMap<String, String>();
	private static Type type = new TypeToken<Map<String, String>>(){}.getType();
	
	@PostConstruct
	public void init() {
		BufferedReader br;
		try {
			br = new BufferedReader(  
			        new FileReader("./sourceToIp.txt"));
			sourceToIp = gson.fromJson(br, type);
			System.out.println("map loaded " + sourceToIp);
			if(sourceToIp == null) 
				sourceToIp = new HashMap<String, String>();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	@GetMapping(value = "/getip")
	public String getip(@RequestParam("source") String source) {		
		return sourceToIp.get(source) == null ? "" : sourceToIp.get(source);
	}
	
	@PutMapping(value = "/putip")
	public void putip(@RequestParam("source") String source, @RequestParam("ip") String ip) {
		String existingIp = sourceToIp.get(source);
		
		if(existingIp == null || !existingIp.equals(ip)) {
			sourceToIp.put(source, ip);
			System.out.println("New ip found " + source + " ip " + ip);
			try(FileWriter writer = new FileWriter("./sourceToIp.txt")) {
				gson.toJson(sourceToIp, type, writer);
				writer.flush();
			} catch (JsonIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
